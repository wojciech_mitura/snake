const SNAKE_CELL_SIZE = 20;
const SNAKE_LENGTH = 3;
const SNAKE_COLOR = '#445424';

window.addEventListener('load', function(e) {
    const startViewSection = document.getElementById('start-section');
    const startViewInstruction = document.getElementById('start-instruction');
    const gameViewSection = document.getElementById('game-section');
    const gameOverViewSection = document.getElementById('game-over-section');

    const startButton = document.getElementById('button-start');
    const gameOverButton = document.getElementById('button-game-over');
    const instructionButton = document.getElementById('button-instruction');
    const arrowButons = document.querySelector('.game__arrows');

    const ctxCanvas = gameViewSection.getContext('2d');
    const canvasWidth = gameViewSection.width;
    const canvasHeight = gameViewSection.height;
    const gameOverText = document.querySelector('.game__over-text');

    const snakeWidth = SNAKE_CELL_SIZE;
    const snakeHeight = (SNAKE_CELL_SIZE / 2);
    const snakeColor = SNAKE_COLOR;
    const gameOverChildren = gameOverViewSection.children;

    const score = document.getElementById('score');

    const gameSection = {
        startViewSection: 0,
        gameViewSection: 1,
        gameOverViewSection: 2
    };

    let activeSection = gameSection.startViewSection;

    const keyCodes = {
        enter: 13,
        rightArrow: 37,
        downArrow: 38,
        leftArrow: 39,
        upArrow: 40
    };

    const directions = {
        right: 0,
        down: 1,
        left: 2,
        up: 3
    };

    let startEnterCount = 0;
    let gameOverEnterCount = 0;
    let animationTimeout;
    let collisionStopInterval;

    const keyConfig = {
        start: {
            onEnterPressed: function(e) {
                if (e.keyCode === keyCodes.enter) {
                    startEnterCount++;

                    if (startEnterCount === 1) {
                        goToInstruction();
                        arrowButons.classList.add('game__arrows--active');
                    } else if (startEnterCount >= 2) {
                        goToGameSection();
                    }
                }
            },
            onStartButtonClick: function() {
                startButton.addEventListener('click', function() {
                    startEnterCount++;
                    goToInstruction();
                    arrowButons.classList.add('game__arrows--active');
                });
            },
            onInstructionButtonClick: function() {
                instructionButton.addEventListener('click', function() {
                    goToGameSection();
                });
            }

        },
        game: {},
        gameOver: {
            onEnterPressed: function(e) {
                if (e.keyCode === keyCodes.enter) {
                    gameOverEnterCount++;
                    clearTimeout(animationTimeout);

                    if (gameOverEnterCount === 1) {
                        gameOverRemoveAnimation();
                    } else if (gameOverEnterCount >= 2) {
                        playAgainGameSection();
                        gameOverEnterCount = 0;
                    }
                }
            }
        },
        reset: function() {
            document.removeEventListener('keydown', keyConfig.start.onEnterPressed);
            document.removeEventListener('keydown', keyConfig.gameOver.onEnterPressed);
        }
    };

    function updateViewConfig() {
        keyConfig.reset();

        switch (activeSection) {
        case gameSection.startViewSection: startViewConfig(); break;
        case gameSection.gameViewSection: gameViewConfig(); break;
        case gameSection.gameOverViewSection: gameOverViewConfig(); break;
        }
    };

/**START SECTION */

    function goToInstruction() {
        startViewInstruction.classList.add('game__start-instruction--active');
    }

    function goToGameSection() {
        startViewInstruction.classList.remove('game__start-instruction--active');
        startViewSection.classList.remove('section__active');
        gameViewSection.classList.add('section__active');
        activeSection = gameSection.gameViewSection;
        updateViewConfig();
    };
/**GAME SECTION */
    function goToGameWin() {
        gameOverViewSection.classList.add('section__active');
        activeSection = gameSection.gameOverViewSection;
        gameOverText.innerHTML = 'you win';
        updateViewConfig();
    }

    function goToGameOverSection() {
        gameOverViewSection.classList.add('section__active');
        activeSection = gameSection.gameOverViewSection;
        updateViewConfig();
        gameOverText.innerHTML = 'game over';
    };

    class Snake {
        body
        direction

        constructor() {
            this.body = [];
            this.direction = directions.right;
            this.setStartBody();
        };

        snakeGrow() {
            this.body.push(this.body[0]);
        };

        setStartBody() {
            for (var i = 0; i < SNAKE_LENGTH; i++) {
                this.body.push({
                    x: 0,
                    y: 6
                });
            };
        };

        getBody() {
            return this.body;
        };

        drawBodyPart(x, y) {
            ctxCanvas.fillStyle = snakeColor;
            ctxCanvas.fillRect(
                x * snakeWidth,
                y * snakeHeight,
                snakeWidth,
                snakeHeight
            );

            ctxCanvas.fillStyle = '#000';
            ctxCanvas.strokeRect(
                x * snakeWidth,
                y * snakeHeight,
                snakeWidth,
                snakeHeight
            );
        };

        draw() {
            ctxCanvas.clearRect(0, 0, canvasWidth, canvasHeight);

            for (var i = 0; i < this.body.length; i++) {
                const x = this.body[i].x;
                const y = this.body[i].y;
                this.drawBodyPart(x, y);
            };
        };

        move() {
            let snakeX = this.body[0].x;
            let snakeY = this.body[0].y;

            this.body.pop();

            if (this.direction === directions.left) snakeX--;
            else if (this.direction === directions.up) snakeY--;
            else if (this.direction === directions.right) snakeX++;
            else if (this.direction === directions.down) snakeY++;

            const newHead = {
                x: snakeX,
                y: snakeY
            };
            this.body.unshift(newHead);
        };

        collision() {
            const head = {
                x: this.body[0].x,
                y: this.body[0].y
            };

            if (
                head.x < 0 ||
                head.y < 0 ||
                head.x >= (canvasWidth / snakeWidth) ||
                head.y >= (canvasHeight / snakeHeight)
            ) {
                gameViewSection.classList.add('game__display-collision-animacion');
                clearInterval(collisionStopInterval);
                goToGameOverSection();
            };
        };

        onArrowButtonClick() {
            arrowButons.addEventListener('click', (e) => {
                const arrowButonsContent = e.target;

                if (arrowButonsContent.classList.contains('arrow-left') && this.direction !== directions.right) {
                    this.direction = directions.left;
                } else if (arrowButonsContent.classList.contains('arrow-up') && this.direction !== directions.down) {
                    this.direction = directions.up;
                } else if (arrowButonsContent.classList.contains('arrow-down') && this.direction !== directions.up) {
                    this.direction = directions.down;
                } else if (arrowButonsContent.classList.contains('arrow-right') && this.direction !== directions.left) {
                    this.direction = directions.right;
                }
            });
        }

        getDirection(e, snakeContext) {
            if (e.keyCode === keyCodes.rightArrow && snakeContext.direction !== directions.right) {
                snakeContext.direction = directions.left;
            } else if (e.keyCode === keyCodes.downArrow && snakeContext.direction !== directions.down) {
                snakeContext.direction = directions.up;
            } else if (e.keyCode === keyCodes.leftArrow && snakeContext.direction !== directions.left) {
                snakeContext.direction = directions.right;
            } else if (e.keyCode === keyCodes.upArrow && snakeContext.direction !== directions.up) {
                snakeContext.direction = directions.down;
            };
        };

        start() {
            this.draw();

            const snakeContext = this;

            snakeContext.onArrowButtonClick();
            document.addEventListener('keydown', function(e) {
                snakeContext.getDirection(e, snakeContext);
            });
        };
    };

    class Food {
        x;
        y;

        constructor() {
            this.randomCoordinates();
        }

        randomCoordinates() {
            this.x = Math.round(Math.random() * (canvasWidth / snakeWidth - 1));
            this.y = Math.round(Math.random() * (canvasHeight / snakeHeight - 1));
        };

        draw() {
            ctxCanvas.fillStyle = snakeColor;
            ctxCanvas.fillRect(
                this.x * snakeWidth,
                this.y * snakeHeight,
                snakeWidth,
                snakeHeight
            );

            ctxCanvas.fillStyle = '#000';
            ctxCanvas.strokeRect(
                this.x * snakeWidth,
                this.y * snakeHeight,
                snakeWidth,
                snakeHeight
            );
        };

        createFirstFood() {
            this.draw();
        };

        getPosition() {
            return {
                x: this.x,
                y: this.y
            };
        };

        start() {
            this.createFirstFood();
        };
    };

    class Game {
        snake = new Snake();
        food = new Food();
        scoreValue = 0;
        countLength = SNAKE_LENGTH;

        start() {
            score.innerText = 'SCORE: ' + this.scoreValue;
            this.snake.start();
            collisionStopInterval = setInterval(() => {
                this.snake.draw();
                this.food.draw();
                this.snake.move();
                this.snake.collision();

                if (this.isSnakeSelfCollide()) {
                    gameViewSection.classList.add('game__display-collision-animacion');
                    clearInterval(collisionStopInterval);
                    goToGameOverSection();
                }

                if (this.isSnakeEatFood()) {
                    this.snake.snakeGrow();
                    this.scoreValue += 10;
                    this.countLength += 1;

                    score.innerText = 'SCORE: ' + this.scoreValue;

                    do {
                        this.food.randomCoordinates();
                    } while (this.isSnakeCollideFood());
                }

                if (this.countLength >= 224) {
                    clearInterval(collisionStopInterval);
                    gameViewSection.classList.add('game__display-collision-animacion');
                    goToGameWin();
                }
            }, 150);
        }

        isSnakeCollideFood() {
            return JSON.stringify(this.snake.getBody()).includes(JSON.stringify(this.food.getPosition()));
        }

        isSnakeEatFood() {
            const bodyPosition = this.snake.getBody();
            const foodPosition = this.food.getPosition();

            if (JSON.stringify(bodyPosition[0]) === JSON.stringify(foodPosition)) {
                return true;
            }
        }

        isSnakeSelfCollide() {
            const bodyPositions = this.snake.getBody();
            const headPosition = bodyPositions.map(bodyPart => JSON.stringify(bodyPart));

            const isCollide = (headPosition.slice(1)).some((bodyPosition) => {
                return bodyPosition === headPosition[0];
            });

            return isCollide;
        }
    };

/**GAME OVER SECTION */
    function gameOverRemoveAnimation() {
        for (let i = 0; i < gameOverChildren.length; i++) {
            gameOverChildren[i].classList.remove('game__over-animation');
        }
    };

    function gameOverRemoveAnimationEnter() {
        animationTimeout = setTimeout(function() {
            gameOverEnterCount++;
            gameOverRemoveAnimation();
        }, 5000);

        document.addEventListener('keydown', keyConfig.gameOver.onEnterPressed);
    };

    function gameOverRemoveAnimationOnClick() {
        gameOverViewSection.addEventListener('click', function() {
            gameOverRemoveAnimation();
            gameOverEnterCount = 1;
        });
    };

    function playAgainGameSection() {
        gameOverViewSection.classList.remove('section__active');
        gameViewSection.classList.remove('game__display-collision-animacion');
        activeSection = gameSection.gameViewSection;
        gameOverRemoveAnimation();
        updateViewConfig();
    };

/**CONFIG */
    function startSnake() {
        updateViewConfig();
    };

    function startViewConfig() {
        keyConfig.reset();
        keyConfig.start.onStartButtonClick();
        keyConfig.start.onInstructionButtonClick();

        document.addEventListener('keydown', keyConfig.start.onEnterPressed);
    };

    function gameViewConfig() {
        keyConfig.reset();

        const game = new Game();
        game.start();
    };

    function gameOverViewConfig() {
        keyConfig.reset();

        for (let i = 0; i < gameOverChildren.length; i++) {
            gameOverChildren[i].classList.add('game__over-animation');
        }

        gameOverRemoveAnimationEnter();
        gameOverRemoveAnimationOnClick();
        gameOverButton.addEventListener('click', playAgainGameSection);
    };

    startSnake();
});
