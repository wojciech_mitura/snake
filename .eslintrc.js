module.exports = {
  env: {
    browser: true,
    es6: true
  },
  "parser": "babel-eslint",
  extends: [
    'standard',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  rules: {
    indent: ["error", 4],
    semi: [2, "always"],
    "space-before-function-paren": ["error", "never"]
  }
}
